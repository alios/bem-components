module.exports = {
    options : {
        boss : true,
        eqeqeq : true,
        evil : true,
        expr : true,
        forin : true,
        immed : true,
        loopfunc : true,
        maxdepth : 4,
        maxlen : 120,
        newcap : true,
        noarg : true,
        noempty : true,
        nonew : true,
        onecase : true,
        quotmark : 'single',
        sub : true,
        supernew : true,
        trailing : true,
        undef : true,
        unused : true
    },

    groups : {
        browserjs : {
            options : {
                browser : true,
                predef : ['modules']
            },
            includes : ['*.blocks/**/*.js'],
            excludes : [
                '**/*.bemjson.js',
                '**/*.deps.js',
                '**/*.node.js',
                '**/*.unit.js'
            ]
        },

        bemjson : {
            options : {
                asi : true,
                maxlen : false
            },
            includes : ['*.blocks/**/*.bemjson.js']
        },

        bemhtml : {
            options : {
                predef : [
                    'apply',
                    'applyCtx',
                    'applyNext',
                    'attrs',
                    'bem',
                    'block',
                    'cls',
                    'content',
                    'def',
                    'elem',
                    'js',
                    'local',
                    'match',
                    'mix',
                    'mod',
                    'mode',
                    'tag'
                ]
            },
            includes : ['*.blocks/**/*.bemhtml']
        },

        bemtree : {
            options : {
                predef : [
                    'apply',
                    'applyCtx',
                    'applyNext',
                    'block',
                    'content',
                    'def',
                    'elem',
                    'match',
                    'mod',
                    'mode',
                    'tag'
                ]
            },
            includes : ['*.blocks/**/*.bemtree']
        },

        nodejs : {
            options : {
                node : true,
                predef : ['modules']
            },
            includes : ['*.blocks/**/*.node.js']
        },

        tests : {
            options : {
                predef : [
                    'after',
                    'afterEach',
                    'before',
                    'beforeEach',
                    'describe',
                    'it',
                    'modules',
                    'require'
                ]
            },
            includes : ['*.blocks/**/*.unit.js']
        },

        depsjs : {
            options : {
                asi : true
            },
            includes : ['*.blocks/**/*.deps.js']
        }
    }
};
