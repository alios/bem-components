modules.define('form', [
    'i-bem__dom', 'querystring', 'functions__throttle'
], function(provide, BEMDOM, qs, throttle, block) {
    provide(block.decl({ modName : 'saveable', modVal : true }, {
        onSetMod : {
            js : {
                inited : function() {
                    this.__base.apply(this, arguments);

                    // загрузить из localStorage сохраненные данные, инициалищировать работу с localStorage
                    this.initSavingData();
                }
            }
        },

        /**
         * Инициализирует работу с localStorage
         */
        // TODO: отрефакторить, много обращений к localStorage
        // TODO: хочется _pending, пока форма не заинитится и заполнится данными (хотя не совсем логично, подумать)
        initSavingData : function() {
            var _this = this,
                localStorageName = this.params.name,
                localStorageData = localStorage.getItem(localStorageName);

            // пробуем распарсить сохраненные в localStorage данные и заполнить ими форму
            if(localStorageData) {
                try {
                    this.setVal(qs.parse(localStorageData));
                } catch (e) {}
            }

            // обновлять данные в localStorage при редактировании формы
            this.on('change', throttle(function() {
                localStorage.setItem(localStorageName, qs.stringify(_this.getVal()));
            }, 100));
        }
    }));
});
