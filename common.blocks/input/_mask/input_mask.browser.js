/** @class input */
modules.define('input', ['i-bem__dom'], function(provide, BEMDOM, block) {
    provide(block.decl({ modName : 'mask', modVal : true }, /** @lends input.prototype */{
        onSetMod : {
            js : {
                inited : function() {
                    this.__base.apply(this, arguments);

                    /**
                     * @type {String}
                     */
                    var _this = this,
                        mask = [
                            function(value) { return '012'.indexOf(value) > -1 ? value : 0; },
                            function(value, index, array) {
                                return '01234'.concat(array[index - 1] < 2 ? '56789' : '').indexOf(value) > -1 ?
                                    value : 0;
                            },
                            ':',
                            function(value) { return '012345'.indexOf(value) > -1 ? value : 0; },
                            function(value) { return '0123456189'.indexOf(value) > -1 ? value : 0; }
                        ],
                        prevCaretPos;

                    this.on('change', function(e, data) {
                        if(data && data._guard) return;

                        var currentValue = _this.getVal(),
                            caretPos = _this._getCaretPosition(),
                            shift = prevCaretPos < caretPos ? 1 : -1,
                            newValue;

                        newValue = mask.map(function(token, index) {
                            return typeof token === 'string' ? token : token(currentValue[index], index, currentValue);
                        }).join('');

                        _this.setVal(newValue, { _guard : true });

                        while(caretPos > 0 && caretPos < mask.length) {
                            // if caret not positioned before separator
                            if(typeof mask[caretPos] !== 'string') break;
                            caretPos += shift;
                        }

                        prevCaretPos = caretPos;
                        _this._setCaretPosition(caretPos);
                    });
                }
            }
        },

        _getCaretPosition : function() {
            return this.__self._getCaretPosition(this);
        },

        _setCaretPosition : function() {
            return this.__self._setCaretPosition.apply(this.__self,
                [this].concat(Array.prototype.slice.apply(arguments)));
        }
    }, /** @lends input */{
        /**
         * Возвращает позицию курсора в инпуте
         * @returns {Number}
         * @private
         */
        _getCaretPosition : function(block) {
            var input = block.elem('control').get(0),
                iCaretPos = 0;

            // IE Support
            if(document.selection) {
                input.focus();

                var oSel = document.selection.createRange();

                oSel.moveStart('character', -input.value.length);
                iCaretPos = oSel.text.length;
            } else if(input.selectionStart || input.selectionStart === 0) iCaretPos = input.selectionStart;

            return iCaretPos;
        },

        /**
         * Устанавливает позицию курсора в инпуте
         * @param {BEM} block
         * @param {Number} caretPos
         * @private
         */
        _setCaretPosition : function(block, caretPos) {
            var input = block.elem('control').get(0);

            if(input.createTextRange) {
                var range = input.createTextRange();

                range.move('character', caretPos);
                range.select();
            } else {
                if(input.selectionStart) {
                    input.focus();
                    input.setSelectionRange(caretPos, caretPos);
                } else {
                    input.focus();
                }
            }
        }
    }));
});
