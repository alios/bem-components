([
    {
        tech : 'js',
        shouldDeps : [
            { block : 'i-bem', tech : 'bemhtml' },
            { block : 'popup', mods : { theme : 'islands' }, tech : 'bemhtml' },
            { block : 'time-line', tech : 'bemhtml' }
        ]
    },
    {
        shouldDeps : [
            'time-line',
            { block : 'functions', elem : 'throttle' },
            { block : 'popup', mods : { autoclosable : true, target : 'anchor', theme : 'islands' } }
        ]
    }
])
