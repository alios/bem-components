/** @class input */
modules.define('input', [
    'i-bem__dom', 'jquery', 'BEMHTML', 'functions__throttle'
], function(provide, BEMDOM, $, BEMHTML, throttle, block) {
    provide(block.decl({ modName : 'type', modVal : 'time' }, /** @lends input.prototype */{
        onSetMod : {
            js : {
                inited : function() {
                    this.__base.apply(this, arguments);

                    var _this = this;

                    this._popup = $(BEMHTML.apply({
                        block : 'popup',
                        mods : { autoclosable : true, target : 'anchor', theme : 'islands' },
                        content : { block : 'time-line', mods : { direction : 'auto' } }
                    })).bem('popup');

                    this._popup.setAnchor(this.domElem);

                    BEMDOM.append($(document.body), this._popup.domElem);

                    this._time = this._popup.findBlockInside('time-line');

                    this._time.on('change', function() {
                        _this.setVal(BEMDOM.blocks['time-line'].toHuman(_this._time.getVal(), 5));
                    });

                    this.on('change', function() {
                        var val = _this.getVal().split(':');

                        this._time.setVal(parseInt(val[0], 10) * 60 + parseInt(val[1], 10), { _guard : true });
                    });

//                    this.on('change', function(e, data) {
//                        if(data && data._guard) return;
//
//                        var m = _this.getVal().match(/(\d)(\d?):?(\d?)(\d?)/);
//
//                        if(!m) {
//                            _this._map.setVal({
//                                x : _this._vertDirection ? undefined : null,
//                                y : _this._vertDirection ? null : undefined
//                            }).repaint();
//
//                            return _this.setVal('', { _guard : true });
//                        }
//
//                        var p = _this._getCaretPosition(_this),
//                            hours = parseInt('' + (m[1] > 0 && m[1] < 3 ? m[1] : 0) +
//                                (m[2] > 0 && m[2] < 7 ? m[2] : 0), 10),
//                            minutes = parseInt('' + (m[3] > 0 && m[3] < 7 ? m[3] : 0) + (m[4] || 0), 10);
//
//                        _this.setVal(('0'+hours).substr(-2) + ':' + ('0'+minutes).substr(-2), { _guard : true });
//                        _this._setCaretPosition(_this, p == 2 ? 3 : p)
//
//                        _this._map.setVal({
//                            x : _this._vertDirection ? undefined : (hours * 60 + minutes) / 1440,
//                            y : _this._vertDirection ? (hours * 60 + minutes) / 1440 : undefined
//                        }).repaint();
//                    });
                }
            },

            focused : function(modName, modVal) {
                this.__base.apply(this, arguments);

                modVal && this._popup.setMod('visible', true);
            }
        }
    }, /** @lends input */{}));
});
