/** @class input */
modules.define('input', [
    'i-bem__dom', 'jquery', 'BEMHTML', 'functions__throttle'
], function(provide, BEMDOM, $, BEMHTML, throttle, block) {
    provide(block.decl({ modName : 'type', modVal : 'date-time' }, /** @lends input.prototype */{
        onSetMod : {
            js : {
                inited : function() {
                    this.__base.apply(this, arguments);

                    var _this = this;
                    
                    this._popup = $(BEMHTML.apply({
                        block : 'popup',
                        mods : { autoclosable : true, target : 'anchor', theme : 'islands' },
                        content : [
                            { block : 'calendar' },
                            { block : 'time-line', js : { nHours : 9 }, mods : { direction : 'vertical' } }
                        ]
                    })).bem('popup');

                    this._popup.setAnchor(this.domElem);

                    BEMDOM.append(this.domElem, this._popup.domElem);

                    this._calendar = this._popup.findBlockInside('calendar');

                    this._calendar.on('change', function() {
                        _this.setVal(BEMDOM.blocks.calendar.toHuman(_this._calendar.getVal()));

//                        _this._popup.delMod('visible');
                    });
                }
            },

            focused : function(modName, modVal) {
                this.__base.apply(this, arguments);

                modVal && this._popup.setMod('visible', true);
            }
        }
    }, /** @lends calendar */{}));
});
