/** @class page */
modules.define('page', ['i-bem__dom'], function(provide, BEMDOM) {
    provide(BEMDOM.decl(this.name, /** @lends page.prototype */{
        onSetMod : {
            js : {
                inited : function() {
                    var drops = this.findBlocksInside('dnd-drop'),
                        basket = drops[0],
                        trash = drops[1];

                    basket.on('drop', function(e, data) {
                        basket.domElem.append(data.dataTransfer.getData('text/html'));
                    });

                    trash.on('drop', function(/*e, data*/) {
//                        drop.domElem.append(data.dataTransfer.getData('text/html'));
                    });

                    this.findBlocksInside('val').forEach(function(block) {
                        block.bindTo('dragstart', function() {

                        });
                    });
                }
            }
        }
    }, /** @lends page */{}));
});
