({
    block : 'page',
    js : true,
    title : 'dnd-drop',
    mods : { theme : 'islands' },
    head : [
        { elem : 'css', url : '_simple.css' }
    ],
    scripts : [
        { elem : 'js', url : '_simple.js' }
    ],
    content : [
        { block : 'dnd-drop' },
        [1, 2, 3, 4, 5, 6, 7, 8, 9].map(function(v) {
            return {
                block : 'val',
                content : v,
                attrs : {
                    draggable : true,
                    ondragstart : 'event.dataTransfer.setData("text/html", this.outerHTML)'
                }
            };
        }),
        { block : 'dnd-drop', mods : { trash : true } }
    ]
})
