modules.define('dnd-drop', ['i-bem__dom', 'jquery'], function(provide, BEMDOM, $) {
    provide(BEMDOM.decl(this.name, {}, {
        live : function() {
            this.__base.apply(this, arguments);

            // расширить $.event, чтобы получить данные о drop-объекте
            $.event.props.push('dataTransfer');

            this.liveBindTo('dragenter dragover dragleave drop', function(e) {
                if(e.type === 'dragenter') {
                    if(!this._dragEnterCurrent) {
                        this._dragEnterCurrent = e.currentTarget;

                        this.setMod('drag', true);
                    }

                    this._dragEnterTarget = e.target;
                }
                if(e.type === 'dragover') {
                    // Necessary. Allows us to drop.
                    e.preventDefault();
                }
                if(e.type === 'dragleave') {
                    if(!this._dragEnterTarget || (this._dragEnterTarget === e.target)) {
                        this._dragEnterCurrent = undefined;

                        this.delMod('drag');
                    }

                    this._dragEnterTarget = null;
                }
                if(e.type === 'drop') {
                    // stops the browser from redirecting.
                    e.preventDefault();

                    this.delMod('drag');

                    this.emit('drop', e);
                }
            });
        }
    }));
});
