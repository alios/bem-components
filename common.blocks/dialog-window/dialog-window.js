modules.define('dialog-window', ['i-bem__dom', 'BEMHTML', 'jquery'], function(provide, BEMDOM, BEMHTML, $) {
    provide(BEMDOM.decl(this.name, {
        onSetMod : {
            js : {
                inited : function() {
                    var _this = this;

                    this.bindTo(this.elem('control', 'type', 'close'), 'click', function() {
                        _this.close(_this);
                    });

                    BEMDOM.blocks.button && BEMDOM.blocks.button.on(this.domElem, 'click', function(e) {
                        var controlType = _this.getMod(_this.findElem(e.target.domElem, 'control'), 'type');

                        ['close', 'reject', 'resolve'].indexOf(controlType) > -1 && _this[controlType](_this);
                    });
                }
            },

            visibility : function(modName, modVal) {
                var paranjaBlock = this.findBlockOn(this.elem('paranja'), 'paranja');

                paranjaBlock && paranjaBlock.setMod(modName, modVal);
            }
        },
        toggle : function() {
            this[this.hasMod('visibility') ? 'open' : 'close']();
        },
        open : function() {
            var _this = this,
                isError = false;

            if(Array.isArray(this._onOpenArray)) {
                isError = this._onOpenArray.some(function(func) {
                    return func() === false;
                });
            }

            if(!isError) {
                if(this.elem('close').length) {
                    this.bindToWin('keyup', function(e) {
                        if(e.keyCode === 27) {
                            _this.close();
                        }
                    });
                }

                this.delMod('visibility');
            }

            return this;
        },
        close : function() {
            var isError = false;

            if(Array.isArray(this._onCloseArray)) {
                isError = this._onCloseArray.some(function(func) {
                    return func() === false;
                });
            }

            if(!isError) {
                this.unbindFromWin('keyup');

                this.setMod('visibility', 'hidden');
            }

            return this;
        },
        resolve : function() {
            var isError = false;

            if(Array.isArray(this._onResolveArray)) {
                isError = this._onResolveArray.some(function(func) {
                    return func() === false;
                });
            }

            // TODO: закрывать окно вручную из кода?
            !isError && this.close(this);

            return this;
        },
        reject : function() {
            var isError = false;

            if(Array.isArray(this._onRejectArray)) {
                isError = this._onRejectArray.some(function(func) {
                    return func() === false;
                });
            }

            !isError && this.close(this);

            return this;
        },
        onOpen : function(func) {
            if($.isFunction(func)) {
                this._onOpenArray = (this._onOpenArray || []).concat(func);
            }

            return this;
        },
        onClose : function(func) {
            if($.isFunction(func)) {
                this._onCloseArray = (this._onCloseArray || []).concat(func);
            }

            return this;
        },
        onResolve : function(func) {
            if($.isFunction(func)) {
                this._onResolveArray = (this._onResolveArray || []).concat(func);
            }

            return this;
        },
        onReject : function(func) {
            if($.isFunction(func)) {
                this._onRejectArray = (this._onRejectArray || []).concat(func);
            }

            return this;
        }
    }, {
        _windowsList : {},
        generateId : function(idsList) {
            var id = (Math.random() * Math.pow(10, 10)).toFixed(0);

            if(!idsList[id]) {
                return id;
            } else {
                this.generateId(idsList);
            }
        },
        open : function(id, bemJson) {
            if(typeof id !== 'object') {
                //if id is id

                if(!bemJson) {
                    //if in arguments only id

                    return this.get(id) && this.get(id).open();

                } else {
                    //if in arguments id and bemJson

                    return (this.get(id) && this.get(id).open()) || this.createNew(id, bemJson).open();

                }

            } else {
                //if id is bemjson

                if(!id) return null;

                return this.createNew(bemJson).open();

            }
        },
        get : function(id) {
            return this._windowsList[id] || null;
        },
        createNew : function(id, bemJson) {
            if(id) {
                if(!bemJson) {
                    bemJson = id;

                    id = this.generateId(this._windowsList);
                }

                bemJson.js = bemJson.js || {};
                bemJson.js.id = id;

                var dialogWindow = $(BEMHTML.apply(bemJson)).bem('dialog-window');

                BEMDOM.append($(document.body), dialogWindow.domElem);

                this._windowsList[id] = dialogWindow;

                return dialogWindow;
            }
        }
    }));
});
