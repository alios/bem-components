({
    block : 'page',
    js : true,
    title : 'Dialog window',
    head : [{ elem : 'css', url : '_simple.css' }],
    scripts : [{ elem : 'js', url : '_simple.js' }],
    mods : { theme : 'islands' }
})
