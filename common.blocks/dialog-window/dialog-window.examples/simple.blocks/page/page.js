modules.define('page', ['i-bem__dom', 'BEMHTML', 'jquery'], function(provide, BEMDOM, BEMHTML, $) {
    provide(BEMDOM.decl(this.name, {
        onSetMod : {
            js : {
                inited : function() {
                    var _this = this,
                        types = ['info', 'warning', 'error', 'success'];

                    types.forEach(function(type) {
                        var button = $(BEMHTML.apply({
                                block : 'button', mods : { size : 'm', theme : 'islands' },
                                text : type
                            })).bem('button'),
                            dialogWindow;

                        BEMDOM.append(_this.domElem, button.domElem);

                        button.bindTo('click', function() {
                            if(!dialogWindow) {
                                dialogWindow = BEMDOM.blocks['dialog-window'].createNew({
                                    block : 'dialog-window',
                                    mods : {
                                        theme : 'default',
                                        type : type,
                                        visibility : 'hidden'
                                    },
                                    title : 'Title',
                                    content : [
                                        'Message',

                                        {
                                            elem : 'controls',
                                            content : [
                                                {
                                                    block : 'button',
                                                    mods : { size : 'm', theme : 'islands' },
                                                    mix : {
                                                        block : 'dialog-window',
                                                        elem : 'control',
                                                        elemMods : { type : 'close' }
                                                    },
                                                    text : 'Close'
                                                }
                                            ]
                                        }
                                    ]
                                });
                            }

                            dialogWindow.open();
                        });
                    });
                }
            }
        }
    }));
});
