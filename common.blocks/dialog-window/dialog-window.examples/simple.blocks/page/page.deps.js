([
    {
        tech : 'js',
        mustDeps : [
            { block : 'i-bem', tech : 'bemhtml' }
        ],
        shouldDeps : [
            { block : 'button', mods : { theme : 'islands' }, tech : 'bemhtml' },
            {
                block : 'dialog-window',
                mods : { theme : 'default' },
                tech : 'bemhtml'
            }
        ]
    },
    {
        mustDeps : [
            { block : 'i-bem', elem : 'dom' }
        ],
        shouldDeps : [
            { block : 'button', mods : { theme : 'islands' } },
            {
                block : 'dialog-window',
                mods : { theme : 'default' }
            }
        ]
    }
])
