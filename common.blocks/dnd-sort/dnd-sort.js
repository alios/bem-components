// TODO: drag image must be empty or __item node
// TODO: stop propagation for drop
// TODO: add up/down arrows
// TODO: add inputs

modules.define('dnd-sort', ['i-bem__dom', 'jquery'], function(provide, BEMDOM, $) {
    provide(BEMDOM.decl(this.name, {}, {
        move : function(target, place, method) {
            if(place[method === 'after' ? 'next' : 'prev']() === target) return;

            target.detach();
            place[method](target);
        },

        live : function() {
            // extend jQuery event-object
            $.event.props.push('dataTransfer');

            this.liveBindTo('item', 'dragstart dragenter dragover drop dragend', function(e) {
                if(e.type === 'dragstart') {
                    this._dragElement = e.currentTarget;

                    e.dataTransfer.dropEffect = 'move';
                    e.dataTransfer.effectAllowed = 'move';

                    this.setMod(e.currentTarget, 'dragging', true);
                }
                if(e.type === 'dragenter') {
                    if(this._dragElement[0] !== e.currentTarget[0]) {
                        this.__self.move(this._dragElement, e.currentTarget,
                            this._dragElement.nextAll().is(e.currentTarget[0]) ? 'after' : 'before');
                    }
                }
                if(e.type === 'dragover') {
                    // Necessary. Allows us to drop.
                    e.preventDefault();
                }
                if(e.type === 'drop') {
                    // stops the browser from redirecting.
                    e.stopPropagation && e.stopPropagation();
                    return false;
                }
                if(e.type === 'dragend') {
                    this.delMod(e.currentTarget, 'dragging');
                }
            });
        }
    }));
});
