([
    {
        tech : 'js',
        shouldDeps : [
            { block : 'i-bem', tech : 'bemhtml' },
            { block : 'popup', mods : { theme : 'islands' }, tech : 'bemhtml' }
        ]
    },
    {
        shouldDeps : [
            'mini-map',
            { block : 'popup', mods : { autoclosable : true, target : 'anchor', theme : 'islands' } }
        ]
    }
])
