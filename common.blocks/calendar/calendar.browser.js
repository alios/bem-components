/** @class calendar */
modules.define('calendar', [
    'i-bem__dom', 'jquery', 'BEMHTML'
], function(provide, BEMDOM, $) {
    provide(BEMDOM.decl('calendar', /** @lends calendar.prototype */{
        onSetMod : {
            js : {
                inited : function() {
                    var _this = this;

                    this._map = this.findBlockInside('mini-map');

                    var monthsHeight = this.elem('months').height(),
                        visibleHeight = this.elem('visible').height(),
                        hiddenHeight = monthsHeight - visibleHeight;

                    this._map.disableAxis('x').setThumbSize({
                        width : '100%',
                        height : _this.elem('list').height() * visibleHeight / monthsHeight
                    }).setVal({ x : 0.5, y : 0 });

                    this._map.on('change', function() {
                        _this.elem('visible').scrollTop(_this._map.getVal().y * hiddenHeight);
                    });

                    this.bindTo('visible', 'scroll', function() {
                        _this._map.setVal({
                            y : _this.elem('visible').scrollTop() / hiddenHeight
                        });
                    });

                    this.bindTo('day', 'click', function(e) {
                        var month = $(e.currentTarget).parent();

                        _this
                            .delMod(_this.elem('day', 'selected', true), 'selected')
                            .setMod($(e.currentTarget), 'selected', true);

                        _this.setVal(('0' + $(e.currentTarget).text()).substr(-2) + '.' +
                            ('0' + month.data('month')).substr(-2) + '.' +
                            month.data('year'));

                        _this._popup.delMod('visible');
                    });
                }
            }
        }
    }, /** @lends calendar */{
        live : function() {
            this.liveInitOnEvent('mousedown');
            return false;
        },

        toHuman : function(date) {
            return ('0' + date.getDay()).substr(-2) + '.' +
                ('0' + date.getMonth()).substr(-2) + '.' +
                date.getFullYear();
        }
    }));
});
