({
    block : 'page',
    js : true,
    title : 'Desktop blocks',
    head : [{ elem : 'css', url : '_simple.css' }],
    scripts : [{ elem : 'js', url : '_simple.js' }],
    mods : { theme : 'islands' },
    content : [
        {
            block : 'calendar',
            js : { date : new Date() }
        }
    ]
})
