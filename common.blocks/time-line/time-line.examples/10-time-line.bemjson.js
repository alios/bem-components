({
    block : 'page',
    js : true,
    title : 'Desktop blocks',
    head : [{ elem : 'css', url : '_10-time-line.css' }],
    scripts : [{ elem : 'js', url : '_10-time-line.js' }],
    mods : { theme : 'islands' },
    content : [
        {
            tag : 'table',
            content : {
                tag : 'tr',
                content : [
                    {
                        tag : 'td',
                        content : [
                            { tag : 'h2', content : 'Горизонтальный контрол' },
                            { block : 'time-line', mods : { direction : 'horizontal' } },
                            { tag : 'h2', content : 'Вертикальный контрол' },
                            { block : 'time-line', mods : { direction : 'vertical' } },
                            { tag : 'h2', content : 'Подстраивающийся под размер окна контрол' },
                            { block : 'time-line', mods : { direction : 'auto' } }
                        ]
                    },
                    {
                        tag : 'td',
                        content : { block : 'input', mods : { size : 'm', theme : 'islands' } }
                    }
                ]
            }
        }
    ]
})
