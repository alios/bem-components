/** @class page */
modules.define('page', ['i-bem__dom'], function(provide, BEMDOM) {
    provide(BEMDOM.decl(this.name, /** @lends page.prototype */{
        onSetMod : {
            js : {
                inited : function() {
                    var input = this.findBlockInside('input');

                    this.findBlocksInside('time-line').forEach(function(block, i, array) {
                        var prev;

                        block.on('change', function() {
                            var val = block.getVal();
                            if(prev === val) return;
                            prev = val;
                            input.setVal(BEMDOM.blocks['time-line'].toHuman(val, 5));
                            array.filter(function(val) { return val !== block; }).forEach(function(block) {
                                block.setVal(val);
                            });
                        });
                    });
                }
            }
        }
    }));
});
