/** @class time-line */
modules.define('time-line', [
    'i-bem__dom', 'functions__debounce'
], function(provide, BEMDOM, debounce) {
    provide(BEMDOM.decl(this.name, /** @lends time-line.prototype */{
        onSetMod : {
            js : {
                inited : function() {
                    var _this = this;

                    this._map = this.findBlockOn('mini-map');
                    this._thumbSize = 100 / this.elem('item').length + '%';

                    if(this.getMod('direction') === 'auto') {
                        BEMDOM.win.resize(debounce((function() {
                            var handler = function() {
                                _this.setMod('direction', BEMDOM.win.height() > BEMDOM.win.width() ?
                                    'vertical' :
                                    'horizontal');
                            };

                            handler();

                            return handler;
                        }()), 100));
                    } else {
                        this.setDirection();
                    }

                    this._map.on('change', function() {
                        _this.setVal(Math.round(_this._map.getVal()[_this._vertDirection ? 'y' : 'x'] * 1440));
                    });

                    this.on('change', function() {
                        var vert = this._vertDirection;

                        this._map.setVal({
                            x : vert ? undefined : this._val / 1440,
                            y : vert ? this._val / 1440 : undefined
                        });
                    });
                }
            },
            direction : function() { this.setDirection(); }
        },

        setDirection : function() {
            var vert = this._vertDirection = this.getMod('direction') === 'vertical';

            this._map.disableAxis(vert ? 'x' : 'y');
            this._map.enableAxis(vert ? 'y' : 'x');

            this._map.setThumbSize({
                width : vert ? '100%' : this._thumbSize,
                height : vert ? this._thumbSize : '100%'
            }).setVal({
                    x : vert ? undefined : this._map.getVal().y,
                    y : vert ? this._map.getVal().x : undefined
                });

            return this;
        },

        getVal : function() { return this._val; },

        setVal : function(minutes) {
            if(this._val === minutes) return this;

            this._val = minutes;

            this.emit('change');

            return this;
        }
    }, /** @lends calendar */{
        live : function() { this.liveInitOnEvent('mousedown'); },

        toHuman : function(value, precision) {
            var roundTo = function(value, inaccuracy) {
                    var quotient = Math.floor(value / inaccuracy),
                        remainder = value % inaccuracy;

                    return (quotient + (remainder > inaccuracy / 2 ? 1 : 0)) * inaccuracy;
                },
                minutes = precision ? roundTo(value, 5) : value;

            return ('0' + Math.floor(minutes / 60)).substr(-2) + ':' +
                ('0' + minutes % 60).substr(-2);
        }
    }));
});
