({
    shouldDeps : [
        { block : 'button', mods : { theme : 'islands', size : 'm' } },
        { block : 'icon', mods : { type : 'cancel' } }
    ]
})
