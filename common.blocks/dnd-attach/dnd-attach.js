// TODO: выводить alert, когда выбраны файлы с неподходящим расширением?
// TODO: очистка не работает в IE

modules.define('dnd-attach', ['i-bem__dom'], function(provide, BEMDOM) {
    provide(BEMDOM.decl(this.name, {
        onSetMod : {
            js : {
                inited : function() {
                    this._emptyFileList = this.elem('input')[0].files;
                    this._name = this.params.name;
                }
            }
        },

        onElemSetMod : {
            img : {
                hidden : {
                    true : function(elem) {
                        elem[0].src = '';
                    }
                }
            },

            preview : {
                big : {
                    true : function() {
                        this.setMod(this.elem('img'), 'hidden', true);
                    }
                },
                empty : {
                    true : function() {
                        this.setMod(this.elem('img'), 'hidden', true);
                    }
                }
            }
        },

        clear : function() {
            this.setVal();

            return this;
        },

        getName : function() {
            return this._name;
        },

        getVal : function() {
            return this._val;
        },

        setVal : function(file) {
            var fileName = file && file.name;

            if(file === this._val) {
                this.setMod(this.elem('preview'), 'empty', true);

                return this;
            }

            this._val = file;

            this.elem('input')[0].files = this._emptyFileList;
            this.updatePreview(file);
            this.findBlockOn(this.elem('icon'), 'icon')
                .setMod('file-extension', fileName ? fileName.split('.')[1].toLowerCase() : '');
            this.elem('name').text(fileName || '');
            this.emit('change', file);

            return this;
        },

        updatePreview : function(file) {
            var _this = this/*,
                fileReader*/;

            this.elem('name').attr('title', file && file.name);

            // показывать превью для файлов меньше 3 МБ
            if(file && file.size < 3 * 1024 * 1024) {
                this.delMod(this.elem('preview'), 'big');

                // using experimental URL.createObjectUrl instead of FileReader
                _this.elem('img')[0].src = window.URL.createObjectURL(file);
                _this.delMod(_this.elem('img'), 'hidden');

                _this.elem('img')[0].onload = function() {
                    window.URL.revokeObjectURL(_this.elem('img')[0].src);
                };

//                fileReader = new FileReader();
//
//                fileReader.onloadend = function(e) {
//
//                    _this.elem('img')[0].src = e.currentTarget.result || '';
//                    _this.delMod(_this.elem('img'), 'hidden');
//                };
//
//                fileReader.readAsDataURL(file);
            } else {
                this.setMod(this.elem('preview'), 'big', !!file || '');
            }

            this.setMod(this.elem('preview'), 'empty', !file || '');

            return this;
        }
    }, {
        live : function() {
            this.liveBindTo('input', 'change', function(e) {
                var file = e.currentTarget[0].files[0];

                file && this.setVal(file);
            });

            this.liveBindTo('clear', 'click', function() {
                this.clear();
            });

            this.liveBindTo('preview', 'click', function() {
                this.elem('input').click();
            });

            this.liveInitOnBlockInsideEvent('drop', 'dnd-drop', function(e, data) {
                this.setVal(data.dataTransfer.files[0]);
            });
        }
    }));
});
