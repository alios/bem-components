modules.define('translit', function(provide){
    //  http://ru.wikipedia.org/wiki/ISO_9
    provide(function(str) {
        var letters = {
                'а' : 'a',    'б' : 'b',    'в' : 'v',    'г' : 'g',    'д' : 'd',
                'е' : 'e',    'ё' : 'yo',   'ж' : 'zh',   'з' : 'z',    'и' : 'i',
                'й' : 'j',    'к' : 'k',    'л' : 'l',    'м' : 'm',    'н' : 'n',
                'о' : 'o',    'п' : 'p',    'р' : 'r',    'с' : 's',    'т' : 't',
                'у' : 'u',    'ф' : 'f',    'х' : 'kh',   'ц' : 'c',    'ч' : 'ch',
                'ш' : 'sh',   'щ' : 'shh',  'ъ' : '``',   'ы' : 'y`',   'ь' : '`',
                'э' : 'e`',   'ю' : 'yu',   'я' : 'ya',
                'А' : 'A',    'Б' : 'B',    'В' : 'V',    'Г' : 'G',    'Д' : 'D',
                'Е' : 'E',    'Ё' : 'Yo',   'Ж' : 'Zh',   'З' : 'Z',    'И' : 'I',
                'Й' : 'J',    'К' : 'K',    'Л' : 'L',    'М' : 'M',    'Н' : 'N',
                'О' : 'O',    'П' : 'P',    'Р' : 'R',    'С' : 'S',    'Т' : 'T',
                'У' : 'U',    'Ф' : 'F',    'Х' : 'Kh',   'Ц' : 'C',    'Ч' : 'Ch',
                'Ш' : 'Sh',   'Щ' : 'Shh',  'Ъ' : '``',   'Ы' : 'Y`',   'Ь' : '`',
                'Э' : 'E',    'Ю' : 'Yu',   'Я' : 'Ya',
                '№' : '#'
            },
            replacer = function(a) { return letters[a] || a; };

        return str.replace(/[А-яёЁ]/g, replacer);
    });
});
