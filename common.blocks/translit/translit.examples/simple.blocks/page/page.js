/** @class page */
modules.define('page', ['i-bem__dom', 'translit'], function(provide, BEMDOM, translit) {
    provide(BEMDOM.decl(this.name, /** @lends page.prototype */{
        onSetMod : {
            js : {
                inited : function() {
                    var input = this.findBlockInside('textarea'),
                        output = this.findBlockInside('translit');

                    input.on('change', function() {
                        output.domElem.html(translit(input.getVal()));
                    });
                }
            }
        }
    }, /** @lends page */{}));
});
